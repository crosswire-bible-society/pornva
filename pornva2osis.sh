#!/bin/bash

wget https://cdn.bibletranslationtools.org//bible/pt-br/ulb/source.zip
unzip source.zip
sed -ri 's/\\s5/\\p/g' pt-br_ulb/*.usfm
u2o.py -e utf-8 -l por -o pornva.osis.xml -v -d PorNVA pt-br_ulb/*.usfm
#mkdir ~/.sword/modules/texts/ztext/pornva/
xmllint --noout --schema ~/.bin/schema/osisCore.2.1.1-cw-latest.xsd pornva.osis.xml
#osis2mod ~/.sword/modules/texts/ztext/pornva/ pornva.osis.xml -z
#cp pornva.conf ~/.sword/mods.d/
#rm source.zip
